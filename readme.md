### fullpage.js & slick

A quick test to see if [fullpage.js](https://github.com/alvarotrigo/fullPage.js) and [slick](https://github.com/kenwheeler/slick) carousel integrate well with each other.

#### Installation & Setup

* Run `npm install`

Serve index.html in your preferred server. On a mac, run `python -m SimpleHTTPServer` and open up `localhost:8000`